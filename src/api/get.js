import axios from "axios";

export const getData = async () => {
  const req = await axios.get(
    "https://bitbucket.org/!api/2.0/snippets/showpo/eLnnrr/6dc6df896912bc91459988097d26fd334d5d1108/files/pdpPayload.json"
  );
  return req.status === 200 ? req.data : undefined;
};
