I wasn’t able to finish the dual output on carousel and its thumbnail navigation. I wanted to load the pdpCarousel but it wasn’t available on vue. I’m looking for a similar component.

I used the existing icon components of Element-UI for the favorites icon. I wouldn’t like to load another icon set for this project because it will make it heavy.

Instructions:
1. npm install
2. npm run dev